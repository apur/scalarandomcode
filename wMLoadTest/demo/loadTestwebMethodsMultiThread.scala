import java.io._
import java.util.Vector
import com.wm.util.Table
import com.wm.data._
import com.wm.util.coder.IDataCodable
import com.wm.app.b2b.util.GenUtil
import com.wm.app.b2b.client.Context
import com.wm.app.b2b.client.ServiceException
import scala.actors.Actor._
import scala.concurrent.ops._
import scala.actors.Actor
import scala.collection.mutable.ArrayBuffer

object loadTestwebMethodsMultiThread extends App {

	val loadnode = xml.XML.loadFile("c:\\wMLoadTest\\info.xml")    
    	val serverDetails = loadnode \\ "ServerDetails"
    	var  server = (serverDetails \ "Server").text
    	var username = (serverDetails \ "UserId").text
    	var password = (serverDetails \ "Password").text        
        val service = loadnode \\ "Services"        
        service(0) match {
        	case <Services>{svcs @ _*}</Services> => {
        	
				for (svc @ <Service>{_*}</Service> <- svcs) {
					
					new Actor { override def act() = {
							val input = svc \ "Input"	     		     			     		        		
							val inputD = addInput(input)
							val nameSpace = (svc \ "Namespace").text	     		     	
							val svcName = (svc \ "Name").text
							val respName = (svc \ "ResponseName").text
							var logger = "Service Number: " + (svc \ "Number").text + "|Name: " + svcName + "|NameSpace: --> " + nameSpace + "|Service Response Dcoument: " + respName;
							actor { 
								for( a <- 1 to (svc \ "Counter").text.toInt) {

												new Actor { override def act() = {
													var context = new Context()							
													context.connect(server, username, password)
													var outP = context.invoke(nameSpace, svcName, inputD );	
													logger += outP.toString
													println(logger)
													//GenUtil.printRec(outP, respName);         		         
													context.disconnect();
												} }.start

								}
							}
					} }.start
				}
		}
        } 
          
	 def addInput(n: scala.xml.NodeSeq): IData = {			
	 		var out =  IDataFactory.create()	 		
			var idc: IDataCursor  = out.getCursor()						
			    n match {
				case Seq(<Input>{inputs @ _*}</Input>) => {					
					doAll(inputs, idc)	
					idc.destroy();
					out
				}
				case Seq(<IData>{idatas @ _*}</IData>) => {															
					doAll(idatas, idc)	
					idc.destroy();
					out					
				}
				case Seq(<IDataList>{idList @ _*}</IDataList>) => {
					doAll(idList, idc)						
					idc.destroy();
					out					
				}
			    }			
			    
	 }
	 def doAll(n: scala.xml.NodeSeq, idc: IDataCursor): Unit = {				
		addString(n, idc)	
		addIData(n, idc)
		addIDataList(n, idc)
		addStringList(n, idc)
	 }
	 def addStringList(n: scala.xml.NodeSeq, idc: IDataCursor): Unit = {
		var idOutList = ArrayBuffer[String]()					
		var counter = 0
		var nameIdlist = ""		 
	 	for (idataString @ <IDataStringList>{_*}</IDataStringList> <- n) {
	 			if (counter == 0) nameIdlist = (idataString \ "Name").text	 			
	 			idOutList += (idataString \ "Value").text
	 			
	 	}
	 	if (idOutList.size != 0) idc.insertAfter(nameIdlist, idOutList.toArray)
	 }
	 def addIDataList(n: scala.xml.NodeSeq, idc: IDataCursor): Unit = {				
		var idOutList = ArrayBuffer[IData]()					
		var counter = 0
		var nameIdlist = ""
		for (idlist @ <IDataList>{_*}</IDataList> <- n) {						
			if (counter == 0) nameIdlist = (idlist \ "Name").text
			var outList: IData =  addInput(idlist)	 		
			counter += 1
			idOutList += outList
		}					
		if (idOutList.size != 0) idc.insertAfter(nameIdlist, idOutList.toArray)
	 }	 
	 def addString(n: scala.xml.NodeSeq, idc: IDataCursor): Unit = {				
	 	for (idataString @ <IDataString>{_*}</IDataString> <- n) idc.insertAfter((idataString \ "Name").text, (idataString \ "Value").text)
	 }
	 
	 def addIData(n: scala.xml.NodeSeq, idc: IDataCursor): Unit = {				
		for (idata @ <IData>{_*}</IData> <- n) {						
		    var name = (idata \ "Name").text					    	
		    idc.insertAfter(name, addInput(idata));    					    					    	    					    
		}
	 }
}