Load Test webMethods using Recursion in Scala
---------------------------------------------
- Abhishek Purohit

Disclaimer -
A. This small program will help you load test your webMethods IS.
B. The amound of load will depend on how powerful your desktop is in hitting webMethods
C. When you start seeing Out Of Memory error on comman prompt that means either your Desktop or the target IS cant handle it. This is your cue to reduce the counter in Info.xml
D. This utility uses DOS command prompt for the load testing. 

SetUp -

1. Install Scala --> http://www.scala-lang.org/downloads2. 
3. Download and extract this zip file in your temp folder.
3. Copy the folder wMLoadTest from the extracted folder and paste it on C:\. So that C:\wMLoadtest is available.
4. Going forward all the load Test work will be carried out from this folder - wMLoadTest.
5. Open the file execute.bat in a Text Editor
6. Please update the path to jar files and folder which are part of standard webMethods installation. 
	They are ->
	a. .\classes - There should be a subfolder classes under folder wMLoadTest
	b. C:\webMethods8\common\lib\wm-isclient.jar -  Please update the path to jar files wm-isclient.jar as per installation on your machine
	c. C:\webMethods8\common\lib\ext\mail.jar -  ditto for mail.jar
	d. C:\SoftwareAG\common\lib\ext - Now this will reference the entire directory which has lot of jar files
	e. 
		
