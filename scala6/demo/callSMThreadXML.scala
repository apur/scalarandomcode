import java.io._
import java.util.Vector
import com.wm.util.Table
import com.wm.data._
import com.wm.util.coder.IDataCodable
import com.wm.app.b2b.util.GenUtil
import com.wm.app.b2b.client.Context
import com.wm.app.b2b.client.ServiceException
import scala.actors.Actor._
import scala.concurrent.ops._

object callSMThreadXML extends App {

	val loadnode = xml.XML.loadFile("c:\\scala6\\info.xml")    
    	val serverDetails = loadnode \\ "ServerDetails"
    	var  server = (serverDetails \ "Server").text
    	var username = (serverDetails \ "UserId").text
    	var password = (serverDetails \ "Password").text        
        val service = loadnode \\ "Services"        
        service(0) match {
        	case <Services>{svcs @ _*}</Services> => {
        	
				for (svc @ <Service>{_*}</Service> <- svcs) {
					spawn {
							val input = svc \ "Input"	     		     			     		        		
							val inputD = addInput(input)
							val nameSpace = (svc \ "Namespace").text	     		     	
							val svcName = (svc \ "Name").text
							val respName = (svc \ "ResponseName").text        		
							actor { 
								for( a <- 1 to (svc \ "Counter").text.toInt) {

												var context = new Context()							
												context.connect(server, username, password)
												var outP = context.invoke(nameSpace, svcName, inputD );							
												GenUtil.printRec(outP, respName);         		         
												context.disconnect();

								}
							}
					}
				}
		}
        } 
          
	 def addInput(n: scala.xml.NodeSeq): IData = {			
	 		var out =  IDataFactory.create()	 		
			var idc: IDataCursor  = out.getCursor()			
			    n(0) match {
				case <Input>{inputs @ _*}</Input> => {
					for (idataString @ <IDataString>{_*}</IDataString> <- inputs) idc.insertAfter((idataString \ "Name").text, (idataString \ "Value").text)
					for (idata @ <IData>{_*}</IData> <- inputs) {						
					    var name = (idata \ "Name").text					    	
					    idc.insertAfter(name, addInput(idata));    					    					    	    					    
					}
					idc.destroy();
					out
					
															

				}
				case <IData>{idatas @ _*}</IData> => {					
					for (idataString @ <IDataString>{_*}</IDataString> <- idatas) idc.insertAfter((idataString \ "Name").text, (idataString \ "Value").text)										
					for (idata @ <IData>{_*}</IData> <- idatas) {						
					    var name = (idata \ "Name").text					    	
				            idc.insertAfter(name, addInput(idata));    					    					    	    					    
					}
					idc.destroy();
					out
					
				}
			    }			
			

	 }
	

}