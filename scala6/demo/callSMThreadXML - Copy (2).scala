import java.io._
import java.util.Vector
import com.wm.util.Table
import com.wm.data._
import com.wm.util.coder.IDataCodable
import com.wm.app.b2b.util.GenUtil
import com.wm.app.b2b.client.Context
import com.wm.app.b2b.client.ServiceException
import scala.actors.Actor._
import scala.concurrent.ops._

object callSMThreadXML extends App {

	val loadnode = xml.XML.loadFile("c:\\scala6\\info.xml")    
    	val serverDetails = loadnode \\ "ServerDetails"
    	var  server = (serverDetails \ "Server").text
    	var username = (serverDetails \ "UserId").text
    	var password = (serverDetails \ "Password").text        
        var ipStart = "20000101"
        var ipEnd = "20121201"	
        val service = loadnode \\ "Services"
        service(0) match {
        	case <Services>{svcs @ _*}</Services> => 
        	for (svc @ <Service>{_*}</Service> <- svcs) {
        		val input = svc \ "IData"	     	
	     		val name = (input \ "Name").text
	     		var out =  IDataFactory.create()
			val inputD = addInput(name, input,out)
			val nameSpace = (svc \ "Namespace").text	     		     	
			val svcName = (svc \ "Name").text
			val respName = (svc \ "ResponseName").text
        		println("Number: " + (svc \ "Number").text)	     		     	
			for( a <- 1 to (svc \ "Counter").text.toInt) {
					      spawn { 
							var context = new Context()
							
							context.connect(server, username, password)
							var outP = context.invoke(nameSpace, svcName, inputD);
							println(outP)
							GenUtil.printRec(outP, respName);         		         
							context.disconnect();
						}
			}
		}
        } 
          
	 def addInput(name: String, n: scala.xml.NodeSeq,out: IData): IData = {
			
			var idc: IDataCursor  = out.getCursor()
			var name = (n \ "Name").text      
			val value = n \ "Value"									
			    n(0) match {
				case <IData>{idatas @ _*}</IData> => {
					var wentIn = 0
					var outR = IDataFactory.create() 
					for (idata @ <IData>{_*}</IData> <- idatas) {				    					    
				            if (wentIn == 0) {
					    outR =  IDataFactory.create()
				            idc = out.getCursor()				            }
				            println("Name:" +name+"  Counter:"+wentIn)
				            println("Idata"+idata)
					    idc.insertAfter(name, addInput((idata \ "Name").text, idata,outR ));    
					    println(outR)
					    wentIn += 1
					}
					
					//if (wentIn == 0) {idc.insertAfter(name, (n \ "Value").text)}
					idc.destroy();
					return out
				}

			    }			

	 }
	

}